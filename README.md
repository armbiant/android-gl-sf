glSF - GL Streaming Format
==========================

glSF is a 3D buffer exchange format, meant to allow for a software to send a 3D scene and its updates over time to another one.


## Motivation

Metalab's software cover a wide range of capabilities and as such are organized as different tools, to respect as much as possible the Unix separation of concerns philosophy. As more and more of our software need to know about the current 3D scene, be it for image rendering, audio simulation, projection mapping, etc., and as this knowledge has to be shared in real-time more often than not, a buffer format for exchanging this data has become necessary.

Although it would be possible to work with sharing data using an existing file format, such a solution would impose the various software to have access to a shared filesystem. This would make 3D scene sharing more complicated when the software do not all run on the same computer which is oftentimes the case, for example to run the video and audio rendering on separate resources. In the case where all software run on the same computer it would also make scene sharing slower, as the whole scene would have to be re-read for every update.

Lastly, defining a buffer exchange format allows for the mean of transmission to be chosen by the user which makes room for a variety of configurations.
